score = float(input('Type a score: '))

def calc_rating(score):
    if 10.0 >= score >= 9.1:
        return 'A'

    if 9.0 >= score >= 8.1:
        return 'A-'

    if 8.0 >= score >= 7.1:
        return 'B'

    if 7.0 >= score >= 6.1:
        return 'B-'

    if 6.0 >= score >= 5.1:
        return 'C'

    if 5.0 >= score >= 4.1:
        return 'C-'

    if 4.0 >= score >= 3.1:
        return 'D'

    if 3.0 >= score >= 2.1:
        return 'D-'

    if 2.0 >= score >= 1.1:
        return 'E'

    if 1.0 >= score >= 0.0:
        return 'E-'

if score > 10 or score < 0:
    print('Nota inválida')
else:
    nota = calc_rating(score)
    print(nota)



