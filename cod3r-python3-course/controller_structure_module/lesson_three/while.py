from random import randint

number = -1
secret_number = randint(0, 9)

while number != secret_number:
    number = int(input('Type a number in range of 0 and 9: '))
    print('Wrong!!!')

print('Secret number {} has been found!'.format(secret_number))